#!/usr/bin/env python3

import argparse
import subprocess

def mount(snap,directory):
    mounted = not subprocess.call("/bin/grep -q '" + snap + "' /etc/mtab", shell=True)
    if not mounted:
        cumpath = [];
        for path in directory.split('/'):
            cumpath.append(path)
            makedir('/'.join(cumpath))
        print("mounting: {} at {}".format(snap,directory))
        return not \
               subprocess.call("mount -t zfs -o ro {} {}".format(snap,directory),
               shell=True)
    else:
        print("already mounted: " + snap)
        return mounted
    
def makedir(pth):
    return not subprocess.call("[ -d {0} ] || mkdir {0}".format(pth),shell=True)

def removedir(pth): #attempts to remove parents as well
    cmd = "[ -d {0} ] && rmdir --ignore-fail-on-non-empty {0}"
    while pth and not subprocess.call(cmd.format(pth),shell=True):
        pth = pth.rpartition('/')[0]

def getdir(root, snap):
    return root + '/' + '/.zfs/'.join(snap.rsplit('@',1))

def getfs(snap):
    return snap.rsplit('@',1)[0]

def snapFilter(fs,snap):
    snapfs = snap.rsplit('@',1).pop(0)
    return fs == snapfs

def getsnapslist(pool,pattern):
    cmd = listSnapshotsCmd.format(pool,pattern)
    return subprocess.getoutput(cmd).splitlines()
    
def listFS(pool):
    cmd = listFilesystemCmd.format(pool)
    return subprocess.getoutput(cmd).splitlines()
    
def unmountsnapshot(snap):
    cmd = "grep {} /etc/mtab"
    mounted,ret = subprocess.getstatusoutput(cmd.format(snap))
    mounted = not mounted;
    where = [line.split()[1] for line in ret.splitlines()]
    if mounted:
        cmd = '/bin/umount {}'
        print('unmounting {}'.format(snap))
        subprocess.call(cmd.format(snap),shell=True)
    return (mounted, where)

def deletesnapshot(snap):
    cmd = deleteSnapshotCmd.format(snap)
    print('deleting {}'.format(snap))
    return not subprocess.call(cmd,shell=True)

newSnapshotCmd = '/sbin/zfs snapshot {}@{}'
listFilesystemCmd = '/sbin/zfs list -rHt filesystem -o name {}'
# the '-S userrefs' below guarantees that "kept" snapshots are in addition
# to any being held. userref'd snapshots will not be deleted, until unheld
listSnapshotsCmd = '/sbin/zfs list -rHt snapshot -S userrefs -s creation \
                 -o name {} | /bin/grep {}'
deleteSnapshotCmd = '/sbin/zfs destroy {}'

parser = argparse.ArgumentParser(description=\
            'Create and Destroy ZFS Snapshots, optionally mounting them')
parser.add_argument('-n','--new',help="create new snapshots for filesystems \
in pool with the name NEW")
parser.add_argument('-d','--delete',help="delete snapshots matching DELETE \
from pool")
parser.add_argument('-k','--keep',default=0,type=int,help="when deleting, keep\
the first KEEP snapshots, sorted by snapshot age")
parser.add_argument('-m','--mount', action='store_true',help="mount the newly\
created snapshot, or mount everything in pool if not creating a new snapshot")
parser.add_argument('-s','--snapdir', help='where to mount snapshots',
                    default='/zfs')
parser.add_argument('-u','--unmount',help="unmount snapshots matching UNMOUNT \
in pool. The name of the pool matches all snapshots, fyi")
parser.add_argument('-c','--cleanup',help="remove mount directory(ies) upon \
unmounting",action='store_true')
parser.add_argument('-e','--exclude',help="Points to file containing list of \
filesystems NOT to snapshot/delete/mount")
parser.add_argument('poolname',help='the name of the pool to operate on')

args = parser.parse_args()

excludefs = [];
if args.exclude:
    with open(args.exclude,'r') as f:
        excludefs = [line.strip() for line in f if line and line[0] != '#']

if args.new:   # make a new snapshot, mount it if specified
    for fs in listFS(args.poolname):
        if fs not in excludefs:
            snap = '{}@{}'.format(fs,args.new)
            print('snapshotting {}'.format(snap))
            subprocess.call(\
                newSnapshotCmd.format(fs,args.new),shell=True)
            if args.mount:
                 mount(snap, getdir(args.snapdir,snap))
        else:
            print('skipping snapshot for {}'.format(fs))
            
elif args.delete:  #delete the specified snapshots (regex style), keep some
    snapshots = getsnapslist(args.poolname, args.delete)
    for fs in listFS(args.poolname):
        if fs in excludefs:
            print('skipping delete from {}'.format(fs))
            continue
        snapsFS = [snap for snap in snapshots if snapFilter(fs,snap)]
        for snap in snapsFS[:-args.keep or len(snapsFS)]:
            (mounted, where) = unmountsnapshot(snap)
            if not deletesnapshot(snap) and mounted:
                # We failed to delete the snapshot, remount!
                [mount(snap,dir) for dir in where]
            elif args.cleanup:
                [removedir(dir) for dir in where]
                
elif args.mount:   # mount everything in the pool
    for snap in getsnapslist(args.poolname, args.poolname):
        if getfs(snap) in excludefs:
            print('skipping mount of {}: excluded fs'.format(snap))
            continue
        mount(snap, getdir(args.snapdir,snap))
        
elif args.unmount: # unmount the matching snapshots
    for snap in getsnapslist(args.poolname, args.unmount):
        if getfs(snap) in excludefs:
            print('skipping unmount of {}: excluded fs'.format(snap))
            continue
        (mounted, where) = unmountsnapshot(snap)
        if args.cleanup:
            [removedir(dir) for dir in where]
